---
permalink:  "/kotlin-your-2017-java-replacement"
title:      "Kotlin – your 2017 Java replacement @ Devoxx Poland 2017"
date:       2017-08-07T08:00:00+02:00
image:      "{{IMAGES_BASE_URL}}/images/covers/kotlin-your-2017-java-replacement.png"
category:   "languages"
tags:       ["jvm-bloggers", "kotlin", "language", "devoxx-pl", "talks"]
description: >
    Rok 2017 to dla mnie rok debiutu konferencyjnego. Na Devoxx Poland
    opowiedziałem o języku Kotlin z perspektywy 8-miesięcznego projektu.
    Zapraszam do obejrzenia mojego wystąpienia 🤓
youtubeVideoUrl: "https://www.youtube.com/embed/dNUxJyPhQ6s"
---

Z czym będzie mi się kojarzył rok 2017? Między innymi z moim
*pierwszym wystąpieniem na scenie dużej konferencji branżowej*.
Był to mój debiut, i to od razu na
[Devoxx Poland](http://devoxx.pl/), nieźle! 😀

Opowiadałem o [Kotlinie](https://kotlinlang.org/) – języku
programowania, który w świecie JVM zyskuje coraz większą 
popularność. Dawniej używany głównie przy tworzeniu aplikacji na 
Androida, dziś jeden z trzech domyślnych języków do
[wygenerowania backendu opartego na Spring Boot](http://start.spring.io/).

*W trakcie prezentacji pokazywałem to, co sprawdziło mi
się w prawdziwym projekcie podczas 8 miesięcy rozwoju backendu
"od zera".* Pokazuję podstawy składni, a następnie opowiadam
o różnych cechach języka, które czynią go wygodnym w użyciu
i pomocnym w codziennych trudach programowania.
Było dla mnie bardzo istotne, aby nie zachwalać tego, 
co wygląda interesująco w syntetycznych przykładach
kodu, lecz ma niewiele wspólnego z tworzeniem
rzeczywistego produktu. 

*Zapraszam Cię do obejrzenia mojego wystąpienia – zamieściłem je tutaj,
na dole* ⬇️ [Jest ono także dostępne na YouTube](https://youtu.be/dNUxJyPhQ6s).
️Jeśli interesują Cię jedynie slajdy (starałem się przygotować je tak,
by możliwie dużo z nich "wyciągnąć" bez oglądania wystąpienia!),
[zajrzyj na Speaker Deck](https://speakerdeck.com/nkoder/kotlin-your-2017-java-replacement).

Miłego oglądania i… daj szansę Kotlinowi 😉
